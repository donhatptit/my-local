$(document).ready(function () {
    $(".btn-back-to-top").click(function () {
        //1 second of animation time
        //html works for FFX but not Chrome
        //body works for Chrome but not FFX
        //This strange selector seems to work universally
        $("html, body").animate({scrollTop: 0}, 1000);
    });

    $(window).scroll(function () {
        if ($('.footer-top').length > 0) {
            const pScrollCuon = $(window).scrollTop();
            const offsetTop = $('.footer-top').offset().top;
            console.log(pScrollCuon >= (offsetTop - (offsetTop * 0.4)))
            if (pScrollCuon >= (offsetTop - (offsetTop * 0.4))) {
                $(".btn-back-to-top").removeClass('hidden');
            } else {
                $(".btn-back-to-top").addClass('hidden');
            }
        }
    });

    $("#close-menu").click(function (event) {
        $('#open-menu').removeClass('open-menu');
        $('#bg-local').removeClass('bg-local-mobile');
        $("#open-menu").stop().css("left", "0").animate({
            left: "-=100%"
        }, 500);
        $('body').css('overflow', 'unset')
        event.stopPropagation();
    });
    $("#menu-mobile").click(function (event) {
        $('#open-menu').addClass('open-menu');
        $('#bg-local').addClass('bg-local-mobile');
        $("#open-menu").stop().animate({
            left: "0%"
        }, 500);
        $('body').css('overflow', 'hidden')
        event.stopPropagation();
    });
    $('#bg-local').not("#open-menu").click(function (event) {
        if ($('#open-menu').hasClass('open-menu') && !$(event.target).is('#open-menu')) {
            $('#open-menu').removeClass('open-menu');
            $('#bg-local').removeClass('bg-local-mobile');
            $("#open-menu").stop().css("left", "0").animate({
                left: "-=100%"
            }, 500);
            $('body').css('overflow', 'unset')
            event.stopPropagation();
        }
    });

    $('.banner-carousel').owlCarousel({
        loop: true,
        margin: 10,
        dots: true,
        nav: true,
        navText: ["<img src='./assets/images/left.svg'>", "<img src='./assets/images/right.svg'>"],
        items: 1
    })

    $('.list-review-desktop').owlCarousel({
        loop: false,
        margin: 30,
        responsiveClass: true,
        nav: false,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: false
            },
            1200: {
                items: 3,
                nav: false,
                loop: false
            }
        }
    });
    $('.list-promotion').owlCarousel({
        loop: false,
        margin: 30,
        responsiveClass: true,
        nav: false,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: false
            },
            1200: {
                items: 3,
                nav: true,
                navText: ["<img src='./assets/images/left.svg'>", "<img src='./assets/images/right.svg'>"],
                loop: false
            }
        }
    });

    $('#list-service-other').owlCarousel({
        loop: false,

        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: false,
                margin: 10,
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: false,
                margin: 30,
            },
            1200: {
                margin: 30,
                items: 4,
                nav: true,
                navText: ["<img src='./assets/images/left.svg'>", "<img src='./assets/images/right.svg'>"],
                dots: false
            }
        }
    });
    $('.list-news').owlCarousel({
        loop: false,
        margin: 30,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: false
            },
            1200: {
                items: 3,
                nav: true,
                navText: ["<img src='./assets/images/left.svg'>", "<img src='./assets/images/right.svg'>"],
                loop: false
            }
        }
    });
    $('.list-experience-mobile').owlCarousel({
        center: true,
        items: 2,
        loop: true,
        dots: false,
        margin: 10,
        responsive: {
            600: {
                items: 4,
                loop: false
            }
        }
    });
});

(function ($) {

    $('body').on('click', '.dropdown_toggle', function (e) {
        var _this = $(this);
        e.preventDefault();
        // _this.toggleClass('submenu');
        _this.parent().next('.submenu').toggleClass('show');
    })

})(jQuery);

var $window = $(window);

function checkWidth() {
    var windowsize = $window.width();
    if (windowsize > 1200) {
        $('#open-menu').removeClass('open-menu');
        $('#bg-local').removeClass('bg-local-mobile');
        $("#open-menu").stop().css("left", "0").animate({
            left: "-=100%"
        }, 500);
        $('body').css('overflow', 'unset')
    }
}

checkWidth();
$(window).resize(checkWidth);

